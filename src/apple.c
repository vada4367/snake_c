#include <stdlib.h>
#include <time.h>

typedef struct
{
  int x;
  int y;
} Apple;

Apple
new_apple (int x, int y)
{
  Apple apple = { x, y };
  return apple;
}

Apple
random_apple (int w, int h)
{
  int x = rand () % w;
  int y = rand () % h;

  return new_apple (x, y);
}
