#define _GNU_SOURCE
#include <unistd.h>
#include "headers/world.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <termios.h>
#include <time.h>

void add_move (int x, int y, int *buffer_moves, int buffer_size);
int *get_remove_last (int *buffer_moves, int buffer_size);

int
main (void)
{
  World world = new_world (10, 10);

  srand (time (NULL));

  struct termios old, new1;
  struct timespec prev_frame_time, now;
  clock_gettime (CLOCK_REALTIME, &prev_frame_time);

  tcgetattr (STDIN_FILENO, &old);
  new1 = old;
  new1.c_lflag &= (~ICANON & ~ECHO);
  tcsetattr (STDIN_FILENO, TCSANOW, &new1);

  int *input_move = malloc (sizeof (int) * 2);
  input_move[0] = 1;
  input_move[1] = 0;

  int buf_size = 16;
  int *buffer_moves = calloc (sizeof (int), buf_size);
  buffer_moves[0] = 1;
  buffer_moves[1] = 0;

  int *move = malloc (sizeof (int) * 2);
  int *unsafe_move = malloc (sizeof (int) * 2);

  for (;;)
    {
      clock_gettime (CLOCK_REALTIME, &now);

      if (1000000000 * (now.tv_sec - prev_frame_time.tv_sec)
                  + (now.tv_nsec - prev_frame_time.tv_nsec)
              > 200000000
          || ((buffer_moves[2] || buffer_moves[3])
              && !(buffer_moves[4] || buffer_moves[5])))
        {
          prev_frame_time = now;

          unsafe_move = get_remove_last (buffer_moves, buf_size);
          if (can_move (world.snake, unsafe_move))
            {
              free (move);
              move = unsafe_move;
            }
          else
            free (unsafe_move);

          if (update (move[0], move[1], &world))
            {
              tcsetattr (STDIN_FILENO, TCSANOW, &old);
              return 69;
            }

          print_world (world);
        }

      if (wasd (input_move))
        add_move (input_move[0], input_move[1], buffer_moves, buf_size);

      usleep (5000);
    }

  return 0;
}

void
add_move (int x, int y, int *buffer_moves, int buffer_size)
{
  for (int i = buffer_size - 1; i >= 2; i--)
    {
      buffer_moves[i] = buffer_moves[i - 2];
    }

  buffer_moves[0] = x;
  buffer_moves[1] = y;
}

int *
get_remove_last (int *buffer_moves, int buffer_size)
{
  int *move = malloc (sizeof (int) * 2);

  int i = 0;
  while ((buffer_moves[i] != 0 || buffer_moves[i + 1] != 0) && i < buffer_size)
    i += 2;

  move[0] = buffer_moves[i - 2];
  move[1] = buffer_moves[i - 1];

  if (i > 3)
    {
      buffer_moves[i - 2] = 0;
      buffer_moves[i - 1] = 0;
    }

  return move;
}
