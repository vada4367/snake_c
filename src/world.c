#include "headers/apple.h"
#include "headers/snake.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  int w;
  int h;
  Snake snake;
  Apple apple;
} World;

World
new_world (int w, int h)
{
  World result = { w, h, new_snake (w / 3, h / 2, w * h),
                   new_apple ((int)((float)w / 1.5), h / 2) };

  return result;
}

void
print_world (World world)
{
  char *frame = malloc (sizeof (char) * (world.w * world.h + 1));

  // Clear frame
  for (int i = 0; i < world.w * world.h; i++)
    {
      frame[i] = '.';
    }

  // Add snake to frame
  for (int i = 0; i < world.snake.length + 1; i++)
    frame[world.snake.body[i * 2] + world.w * world.snake.body[i * 2 + 1]]
        = '#';

  // Add apple to frame
  frame[world.apple.x + world.w * world.apple.y] = '@';

  // Output frame
  printf ("\x1B[H\x1B[J");

  for (int i = 0; i < world.w * 2 + 3; i++)
    {
      if (i % (world.w * 2 + 2) == 0)
        printf ("+");
      else
        printf ("-");
    }
  for (int i = 0; i < world.w * world.h + 1; i++)
    {
      if (i % world.w == 0 && i != 0 && i / world.w != world.h)
        printf (" |\n| ");
      if (i % world.w == 0 && i == 0 && i / world.w != world.h)
        printf ("\n| ");
      if (i / world.w == world.h)
        printf (" |\n");

      if (i % world.w != 0)
        printf (" ");
      printf ("%c", frame[i]);
    }
  for (int i = 0; i < world.w * 2 + 3; i++)
    {
      if (i % (world.w * 2 + 2) == 0)
        printf ("+");
      else
        printf ("-");
    }
  printf ("\n");
  for (int i = 0; i < world.w - 3; i++)
    printf (" ");
  printf ("APPLES: %d\n\n", world.snake.apples);
}

int
update (int dx, int dy, World *world)
{
  if (world->snake.body[0] + dx > world->w - 1 || world->snake.body[0] + dx < 0
      || world->snake.body[1] + dy > world->h - 1
      || world->snake.body[1] + dy < 0)
    return 69;

  if (is_on_snake (world->snake.body[0] + dx, world->snake.body[1] + dy,
                   world->snake, 1))
    return 69;

  if (world->snake.body[0] + dx == world->apple.x
      && world->snake.body[1] + dy == world->apple.y)
    world->snake.length++;

  for (int i = world->snake.length; i > 0; --i)
    {
      world->snake.body[i * 2] = world->snake.body[(i - 1) * 2];
      world->snake.body[i * 2 + 1] = world->snake.body[(i - 1) * 2 + 1];
    }

  world->snake.body[0] += dx;
  world->snake.body[1] += dy;

  if (world->snake.body[0] == world->apple.x
      && world->snake.body[1] == world->apple.y)
    {
      Apple ra;
      do
        {
          ra = random_apple (world->w, world->h);
        }
      while (is_on_snake (ra.x, ra.y, world->snake, 0)
             && world->apple.x != ra.x && world->apple.y != ra.y);

      world->apple = ra;
      world->snake.apples++;
    }

  return 0;
}
