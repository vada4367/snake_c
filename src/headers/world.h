#include "apple.h"
#include "snake.h"

typedef struct
{
  int w;
  int h;
  Snake snake;
  Apple apple;
} World;

World new_world (int w, int h);

int update (int dx, int dy, World *world);

void print_world (World world);
