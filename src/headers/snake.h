
typedef struct
{
  int length;
  int *body;
  int apples;
} Snake;

Snake new_snake (int x, int y, int full_length);

int is_on_snake (int x, int y, Snake snake, int body_from);

char game_input (void);
int wasd (int *input_move);
int can_move (Snake snake, int *direct);
