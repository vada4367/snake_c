#include "headers/apple.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <unistd.h>

typedef struct
{
  int length;
  int *body;
  int apples;
} Snake;

Snake
new_snake (int x, int y, int full_length)
{
  // Malloc 2 length, because snake
  // looks like [[x1, y1], [x2, y2], [x3, y3]]

  Snake snake = { 0, calloc (full_length * 2, sizeof (int)), 0 };

  snake.body[0] = x;
  snake.body[1] = y;

  return snake;
}

int
is_on_snake (int x, int y, Snake snake, int body_from)
{
  // Body from is start of find.
  // If you check apple is_on_snake, you need find
  // on snake apple. If you need find on snake
  // Itself head (for example, check move to itself)
  //
  // Find a x and y in snake body
  // return bool
  for (int i = body_from; i < snake.length; i++)
    {
      if (snake.body[i * 2] == x && snake.body[i * 2 + 1] == y)
        return 1;
    }

  return 0;
}

char
game_input (void)
{
  // Get char with usleep factor
  struct timeval tv = { 0, 0 };
  fd_set fds;

  FD_ZERO (&fds);
  FD_SET (STDIN_FILENO, &fds);
  select (STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
  if (FD_ISSET (STDIN_FILENO, &fds))
    {
      char input = getchar ();
      return input;
    }

  return '\0';
}

int
wasd (int *input_move)
{
  char wasd = game_input ();

  switch (wasd)
    {
    case 'w':
      input_move[0] = 0;
      input_move[1] = -1;
      return 1;

    case 'a':
      input_move[0] = -1;
      input_move[1] = 0;
      return 1;

    case 's':
      input_move[0] = 0;
      input_move[1] = 1;
      return 1;

    case 'd':
      input_move[0] = 1;
      input_move[1] = 0;
      return 1;
    }

  return 0;
}

int
can_move (Snake snake, int move[2])
{
  // Check head on body
  if (snake.body[0] + move[0] == snake.body[2]
      && snake.body[1] + move[1] == snake.body[3])
    return 0;

  return 1;
}
