CC=cc
LD=ld
FORMAT=clang-format
CFLAGS=-std=c99 -O2 -Wall -Wextra -pedantic
BUILD_DIR=./obj_files
SRC_DIR=./src
HEADERS_DIR=${SRC_DIR}/headers
BIN=./bin/main

build:
	${CC} -o ${BUILD_DIR}/apple.o ${CFLAGS}    -c ${SRC_DIR}/apple.c 
	${CC} -o ${BUILD_DIR}/snake.o ${CFLAGS}    -c ${SRC_DIR}/snake.c 
	${CC} -o ${BUILD_DIR}/world.o ${CFLAGS}    -c ${SRC_DIR}/world.c 
	${CC} -o ${BIN} ${CFLAGS} ${SRC_DIR}/main.c ${BUILD_DIR}/snake.o ${BUILD_DIR}/world.o ${BUILD_DIR}/apple.o

fmt:
	${FORMAT} --style=GNU ${SRC_DIR}/main.c > ${SRC_DIR}/main1.c
	${FORMAT} --style=GNU ${SRC_DIR}/snake.c > ${SRC_DIR}/snake1.c
	${FORMAT} --style=GNU ${SRC_DIR}/world.c > ${SRC_DIR}/world1.c
	${FORMAT} --style=GNU ${SRC_DIR}/apple.c > ${SRC_DIR}/apple1.c

	${FORMAT} --style=GNU ${HEADERS_DIR}/snake.h > ${HEADERS_DIR}/snake1.h
	${FORMAT} --style=GNU ${HEADERS_DIR}/world.h > ${HEADERS_DIR}/world1.h
	${FORMAT} --style=GNU ${HEADERS_DIR}/apple.h > ${HEADERS_DIR}/apple1.h

	rm -rf ${SRC_DIR}/main.c ${SRC_DIR}/snake.c ${SRC_DIR}/world.c ${SRC_DIR}/apple.c
	rm -rf ${HEADERS_DIR}/snake.h ${HEADERS_DIR}/world.h ${HEADERS_DIR}/apple.h

	mv ${SRC_DIR}/main1.c ${SRC_DIR}/main.c
	mv ${SRC_DIR}/snake1.c ${SRC_DIR}/snake.c
	mv ${SRC_DIR}/world1.c ${SRC_DIR}/world.c
	mv ${SRC_DIR}/apple1.c ${SRC_DIR}/apple.c

	mv ${HEADERS_DIR}/snake1.h ${HEADERS_DIR}/snake.h
	mv ${HEADERS_DIR}/world1.h ${HEADERS_DIR}/world.h
	mv ${HEADERS_DIR}/apple1.h ${HEADERS_DIR}/apple.h


clean:
	rm -rf ${BUILD_DIR}/*
	rm -rf ${BIN}

run: build
	${BIN}
